const express = require("express");
const router = express.Router();
var moment = require('moment');
moment().format();

router.get("/getNameAndDate", (req, res) => {
  res.send(moment().format('ddd , MMM D , YYYY  h:mm:ss a')).status(200);
});

module.exports = router;