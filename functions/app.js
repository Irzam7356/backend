require('dotenv').config()
const express = require("express");
const app = express();
const http = require("http");
const socketIo = require("socket.io");
const port = process.env.PORT || 4001;
const index = require("./routes/route");
const cors = require('cors');
app.use(cors({ origin: true }));
app.use(index);
const server = http.createServer(app);
const io = socketIo(server); 
let interval;
io.on("connection", (socket) => {
  console.log("New client connected");
  if (interval) {
    clearInterval(interval);
  }
  interval = setInterval(() => getApiAndEmit(socket), 1000);
  socket.on("disconnect", () => {
    console.log("Client disconnected");
    clearInterval(interval);
  });
});
const getApiAndEmit = socket => {
    const response = new Date();
    socket.emit("FromAPI", response);
  };
server.listen(port, () => console.log(`Listening on port ${port}`));


// const express = require("express");
// const http = require("http");
// require('dotenv').config()
// const socketIo = require("socket.io");
// const port = process.env.PORT || 8001;
// const index = require("./index");
// const app = express();
// var moment = require('moment');
// var cors = require('cors')
// app.use(cors())
// app.use(index);
// const server = http.createServer(app);
// const io = socketIo(server);
// let interval;
// io.on("connection", (socket) => {
//   console.log("New client connected");
//   if (interval) {
//     clearInterval(interval);
//   }
//   interval = setInterval(() => getApiAndEmit(socket), 1000);
//   socket.on("disconnect", () => {
//     console.log("Client disconnected");
//     clearInterval(interval);
//   });
// });
// const getApiAndEmit = socket => {
//   const response = new Date();
//   // Emitting a new message. Will be consumed by the client
//   socket.emit("FromAPI", response);
// };

// router.get('/getDateAndTime', (req, res) =>{
//     res.send(moment().format('ddd , MMM D , YYYY  h:mm:ss a')).status(200);
//  });


// moment().format();


// router.get('/nameDate', (req, res) =>{
//      res.send(moment().format('ddd , MMM D , YYYY  h:mm:ss a')).status(200);
//   });

// server.listen(port, () => console.log(`Listening on port ${port}`));